class WordCount extends HTMLElement {
  private count = 0

  constructor() {
    super();
  }

  countWords() {
    if (document.body.textContent) {
      this.count = document.body.textContent.replaceAll("\n", "").trim().split(/\s+/).length
    }
  }

  connectedCallback() {
    this.countWords()

    this.innerHTML = `
      <p>word count of body elements not including web components (yet) 
        <code style="font-size: 24px">${this.count}</code>
      </p>
    `
  }
}

class PopupInfo extends HTMLElement {
  constructor() {
    super();
  }
  // Element functionality written in here
}

class HelloWorld extends HTMLElement {
  connectedCallback() {
    this.innerHTML = "<h1>Hello World.</h1>"
  }
}

customElements.define("word-count", WordCount);
customElements.define("popup-info", PopupInfo);
customElements.define('hello-world', HelloWorld);

